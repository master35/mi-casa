module.exports = {
    en: {
        translation: {
            WELCOME_MSG: `Welcome to Smart House Control`,
            WELCOME_BACK_MSG: 'Welcome back! ',
            REJECTED_MSG: 'No problem. Please say the date again so I can get it right.',
            GREET_MSG: `Light is {{light}} in the {{room}}`,
            MISSING_MSG: `It looks like you haven't set the light. `,
            HELP_MSG: 'I can control light in your house',
            REPROMPT_MSG: `If you're not sure what to do next try asking for help. If you want to leave just say stop. What would you like to do next? `,
            GOODBYE_MSG: 'Goodbye!',
            REFLECTOR_MSG: 'You just triggered {{intent}}',
            FALLBACK_MSG: 'Sorry, I don\'t know about that. Please try again.',
            ERROR_MSG: 'Sorry, there was an error. Please try again.',
            LAUNCH_HEADER_MSG: 'Smart Light control',
            LAUNCH_HINT_MSG: 'Light status', 
            CONTROL_HINT_MSG: 'Turn light on in the kitchen',
            DATA_AVAILABLE: 'Light status available',
            DATA_NOT_AVAILABLE: 'Light status not available'
        }
    },
    es: {
        translation: {
            WELCOME_MSG: `Bienvenido a Smart House Control. `,
            WELCOME_BACK_MSG: 'Bienvenido de nuevo. ',
            REJECTED_MSG: 'No hay problema. Por favor, dime la fecha de nuevo para que pueda hacerlo bien.',
            GREET_MSG: `Luz esta {{light}} en {{room}}. `,
            MISSING_MSG: `Parece que no has puesto la luz. `,
            HELP_MSG: 'Puedo controlar la luz en tu casa. ',
            REPROMPT_MSG: `Si no sabes como continuar intenta pedir ayuda. Si quieres salir solo dí para. Qué quieres hacer? `,
            GOODBYE_MSG: 'Hasta luego!. ',
            REFLECTOR_MSG: 'Acabas de encender {{intent}}. ' ,
            FALLBACK_MSG: 'Lo siento, no se nada sobre eso. Por favor inténtalo otra vez. ',
            ERROR_MSG: 'Lo siento, ha habido un problema. Por favor inténtalo otra vez. ',
            LAUNCH_HEADER_MSG: 'Smart Light control. ',
            LAUNCH_HINT_MSG: 'Estado de la luz. ', 
            CONTROL_HINT_MSG: 'Enciende la luz en la cocina. ',
            DATA_AVAILABLE: 'Estado de la luz disponible. ',
            DATA_NOT_AVAILABLE: 'Estado de la luz no disponible. '
        }
    }
}
